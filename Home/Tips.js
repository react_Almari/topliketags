import React from 'react';
import {StyleSheet, NetInfo,  Text, View, StatusBar, ScrollView, Image, TouchableOpacity,ImageBackground
  ,ActivityIndicator} from 'react-native'; 
  import { Content, Container, Header, Textarea, Body, Right, Title ,Button} from 'native-base';
  import { Constants, SQLite } from 'expo';
  var db = SQLite.openDatabase('db.db7');//starts db1
export default class Tips extends React.Component {

  constructor (props) {
    super (props);
    this.state={allPosts:[],fetched:false, offline:false};
    this.fetchTips=this.fetchTips.bind(this);
    this.insertFunc=this.insertFunc.bind(this);
  }
  componentDidMount(){
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists tips (id integer primary key not null,catId integer,key integer,jetpack_featured_media_url text,title text, date text,content text);'
      );
    });
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {
          console.warn('Internet is connected in Tips' );
          this.fetchTips();
          this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.fetchTips();
            }
          );
          return;
      }
      if(!isConnected)
      {
          console.warn('No Connection Tips');
          db.transaction(
            tx => {
              tx.executeSql('select * from tips', [], (_, { rows }) =>{
              console.warn(JSON.stringify(rows));
              this.setState({allPosts:rows._array,fetched:true, offline:true})}
              );
            },
            null,
            console.log('success'));
      }
    })
  }
  fetchTips(){
  fetch('http://www.topliketags.com/wp-json/wp/v2/posts?categories=30+29&&per_page=100')
    .then(response => response.json())
    .then((response) => {
      console.log('tips');
      this.setState({allPosts:response,fetched:true})
      // console.log(response );
      console.log(response.length);
      for(var i=0;i<response.length;i++){
        this.insertFunc(response[i])
      }
     })
    .catch((e) =>{
      console.log("error:");
      console.log(e);
    });
}
insertFunc(obj){
  console.log(obj.id);
  db.transaction(tx => {
    tx.executeSql(
      `select * from tips where key = ?;`,
      [obj.id],
      (_, { rows : { _array } })  =>
      {
        // console.log(_array )
        console.log(_array.length)
         if(_array.length>0){
          //  console.log('no insertion => @ ',key)
         }
           if(_array.length==0){
             console.log(catId, 'insert tips @id => ',obj.id)
          db.transaction(
            tx => {
              tx.executeSql('insert into tips (key, content, catId, title, jetpack_featured_media_url, date) values (?, ?,?, ?,?, ?)',
              [obj.id, obj.content.rendered, catId, obj.title.rendered, obj.jetpack_featured_media_url, obj.date]);            
            },
          );
        }
      }
    );
  });
}
  render () { 
   
    
    return (
      <View style={styles.container}>
        <Container style={[styles.container,{backgroundColor:'#ebebeb'}]  }>
          <Content padder style={styles.container}>
          <View  style={{flex:1 }}>
                <View padder style={{flex:1,flexWrap:'wrap',flexDirection:'row'
                ,justifyContent:'space-between',alignItems:'center'}}>
                {this.state.fetched===false && 
                  this.state.allPosts.length==0 &&
                  <ActivityIndicator style={{justifyContent:'center',width:'100%'}} size="large" color="#2fc2e3"/>}
                  {this.state.fetched!==false && 
  this.state.allPosts.length==0 && <Text>No Posts Yet...</Text>}
                {this.state.fetched!==false && 
  this.state.allPosts.length!=0 && this.state.allPosts.map(function(item,key){
    return(<TouchableOpacity key={key} 

      
      onPress={() => this.props.navigation.navigate ('CaptionPost', { postId: this.state.offline==false? item.id:item.key  ,
        catName: this.state.offline==false? item.id:item.key ==29?'Instagram Updates':'Instagram Tips',
      tips:true})}
        style={{height:150,width:'48%',backgroundColor:'#ebebeb',marginTop:7,
        elevation:4,
        shadowOffset: { width: 5, height: 5 },
        shadowColor: "grey",
        shadowOpacity: 0.5,padding:5,
        shadowRadius: 10,}}
    ><ImageBackground
      style={styles.sliderImage} 
      source={{uri:item.jetpack_featured_media_url}}
    ></ImageBackground><Text>{this.state.offline==false? item.title.rendered:item.title}</Text>
    </TouchableOpacity>
    );},this)
          }
               
                </View>
              </View>
          </Content>
        </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  sliderImage: {width: '100%', height: 100},
});