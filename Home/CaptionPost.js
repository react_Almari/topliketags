import React from 'react';import HTML from 'react-native-render-html';
import {StyleSheet,NetInfo, Text, View, StatusBar, Image, Dimensions, TouchableOpacity,ImageBackground
  ,ActivityIndicator} from 'react-native'; import {
    Header,
    Left,
    Body,
    Footer,
    FooterTab,
    Button,
    Item,
    Input,
    Icon,
    Card,
    CardItem,
    Title,
    Container,
    Content,
  } from 'native-base';
  import { Constants, SQLite } from 'expo';
  var db = SQLite.openDatabase('db.db7');
  var postId=null;
  var catName=null;
var post=[];
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
  export default class CaptionPost extends React.Component {
    static navigationOptions = {
        header: null ,
      };
  constructor (props) {
    super (props);
    this.state={isReady: false,fetched:false, offline:false};
    this.fetchPost=this.fetchPost.bind(this);
  }
  async componentWillMount () {
    await Expo.Font.loadAsync ({
      Roboto: require ('../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({isReady: true});
  }
  componentWillUnmount(){
    post=[]
  }
  componentDidMount(){

    var { navigation } = this.props;
    catName = navigation.getParam('catName' ) 
    postId = navigation.getParam('postId' ) 
    // this.fetchData();

    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {
          console.warn('Internet is connected',postId);
          this.fetchPost();
          this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.fetchPost();
            }
          );
          return;
      }
      if(!isConnected)
      { 
        console.warn('No Connection',postId);
        console.warn('params => ',navigation.getParam('tips' ))
        if(navigation.getParam('tips' )==true){

          db.transaction(
            tx => {
              tx.executeSql('select * from tips where key=?', [postId], (_, { rows }) =>{
              console.warn(JSON.stringify(rows));post=rows._array[0];
              this.setState({ fetched:true, offline:true})
              
            }
              );
            },
            null,
            console.log('success'));
        }
        if(navigation.getParam('tips' )!=true){

          db.transaction(
            tx => {
              tx.executeSql('select * from posts where key=?', [postId], (_, { rows }) =>{
              console.warn(JSON.stringify(rows));post=rows._array[0];
              this.setState({ fetched:true, offline:true})
              
            }
              );
            },
            null,
            console.log('success'));
          }
      }
    })
  }
  fetchPost(){
  fetch(`http://www.topliketags.com/wp-json/wp/v2/posts/${postId}`)
    .then(response => response.json())
    .then((response) => {
      console.log('postId');
      post=response;
      this.setState({fetched:true})
      console.log(response.id );
      console.log(response.length);
     })
    .catch((e) =>{
      console.log("error:");
      console.log(e);
    });
}
  render () { 
    console.warn('post,',post);
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <View style={styles.container}>
      <Header hasSegment style={{backgroundColor: '#2fc2e3'}}>
      <Left>
        <Button onPress={() => this.props.navigation.goBack()}
         transparent>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Details</Title>
      </Body>
    </Header>
    <Container style={{flex:1, backgroundColor:'#ebebeb'}   }>
      <Content padder style={{flex:1,}}> 

      {this.state.fetched===false && post.length==0 && 
        <ActivityIndicator style={{justifyContent:'center',width:'100%'}} 
          size="large" color="#2fc2e3"/>}
           {this.state.fetched!==false && 
        post.length==0 &&  <Text>No data found</Text>}
      {this.state.fetched!==false && 
        post.length!=0 &&  

        <Content padder> 
       {catName!='fetch' && <View style={{  alignSelf: 'flex-start' }}>
          <Text  
              style={{backgroundColor:'#2fc2e3',fontWeight:'500',fontSize:16
                      ,borderColor:'#2fc2e3',borderRadius:4, padding:3,width:'auto',
                      borderWidth:1}}
            >{'   '+catName+'   '}</Text>
        </View>}
       <View/>
        <Text style={{fontWeight:"500",fontSize:21,marginTop:10}}>{this.state.offline==false? post.title.rendered:post.title}</Text>
        <Text style={{fontWeight:"400",fontSize:16, color:'grey',marginTop:10}}
        >{monthNames[new Date(post.date).getMonth()]+'-'+new Date(post.date).getDate()+
        '-'+new Date(post.date).getFullYear()}</Text>
        <Image
          style={styles.sliderImage} 
          source={{uri:post.jetpack_featured_media_url}}
       />
        {/* <Text style={{fontWeight:"400",fontSize:18,marginTop:10}}>{post.content.rendered}</Text> */}
         <HTML html={this.state.offline==false? post.content.rendered:post.content} imagesMaxWidth={Dimensions.get('window').width} 
        ignoredStyles={['display', 'width', 'height', 'font-family', 'padding',
        'transform']}/> 
        </Content>                
      }
                
          </Content>
        </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop:StatusBar.currentHeight
  },
  sliderImage: {width: '100%', height: 200,
  resizeMode: 'contain',marginTop:15},
});