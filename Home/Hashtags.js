import React from 'react';
import {StyleSheet, Text, View, Clipboard, Image, ActivityIndicator,TouchableOpacity
  ,ToastAndroid} from 'react-native'; 
  import { Container, Header,Icon,  Content, Accordion } from "native-base";
  const cheerio = require('react-native-cheerio');  
  var headerList=[];
  var parseIt=null;var htmlString='';
  export default class Hashtags extends React.Component {
  
  static navigationOptions = {
    header: null 
  };
  constructor (props) {
    super (props);
    this.state={isReady: false,fetched:false};
    this.fetchHash=this.fetchHash.bind(this);
  }
  async componentWillMount () {
    await Expo.Font.loadAsync ({
      Roboto: require ('../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({isReady: true});
  }
  componentWillUnmount(){
    headerList=[];
  }
  componentDidMount(){

    var { navigation } = this.props;
    catId = navigation.getParam('catId' )
    catName = navigation.getParam('catName' ) 
    // this.fetchData();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.fetchHash();
      }
    );
  }
  async fetchHash(){
    //header Name => $('div .items').children().eq(39).text()
    //number of subheadings => $('div .items').children().eq(1).children().length
    const searchUrl ='http://www.topliketags.com/top-instagram-hashtags/';
    const response = await fetch(searchUrl); 
    console.log('htmlssssssssssssssssss',)
    headerList=[];
    htmlString = await response.text(); 
    console.log('htmlString')
    var $ = cheerio.load(htmlString)
    var iterations= $('div .items').children().eq(1).children().length;
    
    var headerIterations=$('div .items').children().length;
    var headerObj={};
    for(var i=0;i<headerIterations/2;i++){
      headerObj= {
        key: i*2+1,
        title : $('div .items').children().eq(i*2).text(),
        content:$('div .items').children().eq(i*2+1).html()
      };
      console.log(headerObj );
      headerList.push(headerObj);
    }
    // console.log( $('div .items').children().eq(0).text())
    // for(var i=0;i<iterations;i++){
    //   parseIt=$('div .items').children().eq(1).children().eq(i).text();
    //   console.log(parseIt.split("Copy")[0] + " => "+parseIt.split("Copy")[1] );
    // }
   
    console.log('parseIt');
    this.setState ({fetched: true});
  }
_renderContent(item) {
  console.log('parseIt');
  console.log('ssss');
  
  var $ = cheerio.load(htmlString)
  // for(var i=0;i<iterations;i++){
    var iterations=$('div .items').children().eq(item.key).children().length;
    let data=[];
    for(var i=0;i<iterations;i++){
      data.push(i);
    //  var printIt=$('div .items').children().eq(item.key).children().eq(i).text();
    //   console.log(printIt.split("Copy")[0] + " => "+printIt.split("Copy")[1] );
      
    }
    console.log('datadatadatadatadatadatadata' );
    console.log(data );

  // }
  return (
  <View style={{backgroundColor:'#e3f1f1',paddingTop:7}}>
  {data.length!=0 && data.map(function(items,key){

var printIt=$('div .items').children().eq(item.key).children().eq(items).text();
console.log(printIt.split("Copy")[0] + " => "+printIt.split("Copy")[1] );
    return(<View key={key}><TouchableOpacity onPress={()=>{
      Clipboard.setString(printIt.split("Copy")[1])
      ToastAndroid.show('Text Copied to Clipboard !', ToastAndroid.SHORT)
    }}
    style={{flexDirection:'row'}}><Text
    style={{
      backgroundColor: "#e3f1f1",
      padding: 10,fontWeight:'500',fontSize:17
    }}
  >{printIt.split("Copy")[0]}</Text><Icon 
    name="copy" type="FontAwesome" style={{color:'grey'}} /></TouchableOpacity><Text
    style={{
      backgroundColor: "#e3f1f1",
      padding: 10,fontSize:16
    }}
  >{printIt.split("Copy")[1]}</Text></View>)},this)}</View>
  );
}
render () { 
if (!this.state.isReady) {
  return <Expo.AppLoading />;
}
return (
   
      <View style={styles.container}>
        <Container> 
        <Content padder>
        {this.state.fetched===false && 
                  headerList.length==0 &&
                  <ActivityIndicator style={{justifyContent:'center',width:'100%'}} 
                  size="large" color="#2fc2e3"/>}
                  {this.state.fetched!==false && 
  headerList.length==0 && <Text>No Posts Yet...</Text>}
         {this.state.fetched!==false && 
  headerList.length!=0 && <Accordion
            dataArray={headerList}
            renderContent={this._renderContent}
            headerStyle={{ backgroundColor: "#70a0ff" }}
            contentStyle={{ backgroundColor: "#ddecf8" }}
          />}
        </Content>
      </Container> 
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1, 
  },
});