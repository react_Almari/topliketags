import React from 'react';
import {Font} from 'expo';
import {StyleSheet, Text, View,TouchableOpacity, StatusBar ,ImageBackground, Clipboard,
    ToastAndroid,ActivityIndicator} from 'react-native'; 
import {
    Header,
    Left,
    Body,
    Footer,
    Right,
    Button,
    Item,
    Input,
    Icon,
    Card,
    CardItem,
    Title,
    Container,
    Content,
  } from 'native-base';
  import { Constants, SQLite } from 'expo';
  import { DrawerActions } from 'react-navigation'
  var db = SQLite.openDatabase('db.db7');//starts db1
var catId=null;
var catName=null;
  export default class Favorite extends React.Component {
    static navigationOptions = {
        header: null 
      };
  constructor (props) {
    super (props);
    this.state={isReady: false,allPosts:[],fetched:false};
    this.fetchTips=this.fetchTips.bind(this);
    this.remove=this.remove.bind(this);
  }
  async componentWillMount () {
    await Expo.Font.loadAsync ({
      Roboto: require ('../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({isReady: true});
  }
  remove(key){
    db.transaction(
      tx => {
        tx.executeSql(`delete from favorite where key = ?;`, [key]);
      },
      null,
      this.fetchTips()
    );
    db.transaction(
      tx => {
        tx.executeSql('select * from favorite  ', [], (_, { rows }) =>{
        console.warn(JSON.stringify(rows));
        this.setState({allPosts:rows._array,fetched:true})}
        );
      },
      null,
      console.log('success'));
  }
  componentDidMount(){

    var { navigation } = this.props;
    catId = navigation.getParam('catId' )
    catName = navigation.getParam('catName' ) 
    // this.fetchData();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.fetchTips();
      }
    );
  }
  fetchTips(){
    db.transaction(
      tx => {
        tx.executeSql('select * from favorite  ', [], (_, { rows }) =>{
        console.warn(JSON.stringify(rows));
        this.setState({allPosts:rows._array,fetched:true})}
        );
      },
      null,
      console.log('success'));
}
  render () { 
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
    <View style={styles.container}>
        <Header style={{backgroundColor: '#2fc2e3',
          borderBottomColor: '2fc2e3',
          color:"white",shadowColor: 'transparent',
          borderBottomWidth: 0, elevation:0,}}>
            <Left>
                <Button  onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}
             transparent>
                <Icon type="Ionicons"
            name="md-menu" style={{color:'white'}}
            size={24}/>
                </Button>
            </Left>
            <Body>
                <Title style={{fontWeight:'500',marginLeft:15,fontSize:21}}>Favorites</Title>
            </Body>
            <Right>
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate ('HomeScreen')}><Icon
                type="FontAwesome"
                name="home"
                style={{color:'white'}}
                size={24}
            /></TouchableOpacity>
            </Right>
        </Header>
        <Container style={{flex:1, backgroundColor:'#ebebeb'}   }>
          <Content padder style={{flex:1,}}>
          <View  style={{flex:1 }}>
                <View padder style={{flex:1,flexWrap:'wrap',flexDirection:'row'
                ,justifyContent:'space-between',alignItems:'center'}}>
                {this.state.fetched===false && 
                  this.state.allPosts.length==0 &&
                  <ActivityIndicator style={{justifyContent:'center',width:'100%'}} 
                  size="large" color="#2fc2e3"/>}
                  {this.state.fetched!==false && 
  this.state.allPosts.length==0 && <Text>No Favorites Yet...</Text>}
                {this.state.fetched!==false && 
  this.state.allPosts.length!=0 && this.state.allPosts.map(function(item,key){
    return(<TouchableOpacity key={key} 
        onPress={() => this.props.navigation.navigate ('CaptionPost', { postId: item.key ,
        catName:'fetch'})}
      style={{height:270,width:'99%',backgroundColor:'#ebebeb', 
}}
    >
    <Card style={{flex:1,borderRadius:8}}><Content padder>
    <ImageBackground
      style={styles.sliderImage} 
      source={{uri:item.jetpack_featured_media_url}}
    ></ImageBackground><Text style={{fontWeight:"500"}}>{item.title}</Text>
    </Content>
    <Footer >
        <View style={{flex:1,flexDirection:'row',
            justifyContent:'space-between' ,backgroundColor:'#ebebeb'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-start'}}>
            </View>
        <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
            <TouchableOpacity style={{marginRight:10}} onPress={()=>{
              Clipboard.setString(item.title)
              ToastAndroid.show('Text Copied to Clipboard !', ToastAndroid.SHORT)
            }}>
                <Icon name="copy" type="FontAwesome" style={{color:'grey'}}/>
            </TouchableOpacity>
                <TouchableOpacity style={{marginRight:10}} onPress={()=>this.remove(item.key)}>
                    <Icon name="trash" type="FontAwesome" style={{color:'grey'}}/>
                </TouchableOpacity>
        </View>
        {/* <View style={{flex:1}}></View> */}
    </View></Footer></Card>
    </TouchableOpacity>
    );},this)
          }
               
                </View>
              </View>
          </Content>
        </Container>
    </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#2fc2e3',
    paddingTop:StatusBar.currentHeight
  },
  sliderImage: {width: '100%', height: 160},
});