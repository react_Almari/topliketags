import React from 'react';
import {StyleSheet,NetInfo, Text, View, StatusBar, Clipboard, ToastAndroid, TouchableOpacity,ImageBackground
  ,ActivityIndicator} from 'react-native'; import {
    Header,
    Left,
    Body,
    Footer,
    FooterTab,
    Button,
    Item,
    Input,
    Icon,
    Card,
    CardItem,
    Title,
    Container,
    Content,
  } from 'native-base';
import { Constants, SQLite } from 'expo';
var db = SQLite.openDatabase('db.db7');//starts db1
var catId=null;
var catName=null;
  export default class CaptionCategory extends React.Component {
    static navigationOptions = {
        header: null 
      };
  constructor (props) {
    super (props);
    this.state={isReady: false,allPosts:[],fetched:false, offline:false, favList:[]};
    this.fetchTips=this.fetchTips.bind(this);
    this.insertFunc=this.insertFunc.bind(this);
    this.heart=this.heart.bind(this);
  }
  async componentWillMount () {
    await Expo.Font.loadAsync ({
      Roboto: require ('../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({isReady: true});
  }
  componentDidMount(){
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists posts (id integer primary key not null,catId integer,key integer,jetpack_featured_media_url text,title text, date text,content text);'
      );
      tx.executeSql(
        'create table if not exists favorite (id integer primary key not null,key integer,catId integer,jetpack_featured_media_url text,title text, date text,content text);'
      );
    });
    db.transaction(
      tx => {
        tx.executeSql('select * from favorite  ', [], (_, { rows }) =>{
        console.warn(JSON.stringify(rows));
        this.setState({favList:rows._array })}
        );
      },
      null,
      console.log('success'));

    var { navigation } = this.props;
    catId = navigation.getParam('catId' )
    // catId=77;
    catName = navigation.getParam('catName' ) 
    console.warn('catId=>',catId,"ctaname=> ",catName)
    // catName=undefined;
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {
          console.warn('Internet is connected in Posts', catId);
          this.fetchTips();
          this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.fetchTips();
            }
          );
          return;
      }
      if(!isConnected)
      {
          console.warn('No Connection', catId);
          db.transaction(
            tx => {
              tx.executeSql('select * from posts where catId=?', [catId], (_, { rows }) =>{
              console.warn(JSON.stringify(rows));
              this.setState({allPosts:rows._array,fetched:true, offline:true})}
              );
            },
            null,
            console.log('success'));
      }
    })
  }
  heart(obj){

    NetInfo.isConnected.fetch().then(isConnected => {
    if(isConnected)
    {
      console.log(obj.id,'obj.id',obj)
      db.transaction(tx => {
        tx.executeSql(
          `select * from favorite where key = ?;`,
          [obj.id],
          (_, { rows : { _array } })  =>
          {
            // console.log(_array )
            console.log(_array.length)
            if(_array.length>0){
              console.log('already in Favorites',obj.id)
            }
              if(_array.length==0){
                console.log(catId, 'insert favorite @id => ',obj.id)
              db.transaction(
                tx => {
                  tx.executeSql('insert into favorite (key, content, catId, title, jetpack_featured_media_url, date) values (?, ?,?, ?,?, ?)',
                  [obj.id,  obj.content.rendered,catId,   obj.title.rendered, obj.jetpack_featured_media_url, obj.date]);            
                },
              );
              db.transaction(
                tx => {
                  tx.executeSql('select * from favorite  ', [], (_, { rows }) =>{
                  console.warn(JSON.stringify(rows));
                  this.setState({favList:rows._array })}
                  );
                },
                null,
                console.log('success'));
            }
          }
        );
      });
      return;
    }
    if(!isConnected){
      console.warn("Internet connection is required!");
      return;
    }
  });
  }
  fetchTips(){
    console.log('fetchTips');
  fetch(`http://www.topliketags.com/wp-json/wp/v2/posts?categories=${catId}&&per_page=${100}`)
    .then(response => response.json())
    .then((response) => {
      console.log('tips');
      this.setState({allPosts:response,fetched:true})
      // console.log(response );
      console.log(response.length);
      for(var i=0;i<response.length;i++){
        this.insertFunc(response[i])
      }
     })
    .catch((e) =>{
      console.log("error:");
      console.log(e);
    });
}
insertFunc(obj){
  console.log(obj.id);
  db.transaction(tx => {
    tx.executeSql(
      `select * from posts where key = ?;`,
      [obj.id],
      (_, { rows : { _array } })  =>
      {
        // console.log(_array )
        console.log(_array.length)
         if(_array.length>0){
          //  console.log('no insertion => @ ',key)
         }
           if(_array.length==0){
             console.log(catId, 'insert post @id => ',obj.id)
          db.transaction(
            tx => {
              tx.executeSql('insert into posts (key, content, catId, title, jetpack_featured_media_url, date) values (?, ?,?, ?,?, ?)',
              [obj.id, obj.content.rendered, catId, obj.title.rendered, obj.jetpack_featured_media_url, obj.date]);            
            },
          );
        }
      }
    );
  });
}
  render () { 
    console.warn('favlist => ',this.state.favList)
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <View style={styles.container}>
      <Header hasSegment style={{backgroundColor: '#2fc2e3'}}>
      <Left>
        <Button onPress={() => this.props.navigation.goBack()}
         transparent>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Posts</Title>
      </Body>
    </Header>
        <Container style={{flex:1, backgroundColor:'#ebebeb'}   }>
          <Content padder style={{flex:1,}}>
          <View  style={{flex:1 }}>
                <View padder style={{flex:1,flexWrap:'wrap',flexDirection:'row'
                ,justifyContent:'space-between',alignItems:'center'}}>
                {this.state.fetched===false && 
                  this.state.allPosts.length==0 &&
                  <ActivityIndicator style={{justifyContent:'center',width:'100%'}} 
                  size="large" color="#2fc2e3"/>}
                  {this.state.fetched!==false && 
  this.state.allPosts.length==0 && <Text>No Posts Yet...</Text>}
                {this.state.fetched!==false && 
  this.state.allPosts.length!=0 && this.state.allPosts.map(function(item,key){
    var title=this.state.offline==false? item.title.rendered:item.title;
    if(title.length > 90) {  title = title.substring(0,90);
     title= title.concat(" ...");
  }

    var heart=[];
    var id=this.state.offline==false? item.id:item.key;
    heart=this.state.favList.filter(fav => fav.key ==  id );
    console.log('heart',heart)
    return(<TouchableOpacity key={key} 
        onPress={() => this.props.navigation.navigate ('CaptionPost', { postId:this.state.offline==false? item.id:item.key ,
        catName:catName})}
      style={{height:270,width:'99%',backgroundColor:'#ebebeb', 
}}
    >
    <Card style={{flex:1,borderRadius:8}}><Content padder>
    <ImageBackground
      style={styles.sliderImage} 
      source={{uri:item.jetpack_featured_media_url}}
    ></ImageBackground><Text style={{fontWeight:"500"}}>{title}</Text>
    </Content>
    <Footer >
        <View style={{flex:1,flexDirection:'row',
            justifyContent:'space-between' ,backgroundColor:'#ebebeb'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-start'}}>
                <TouchableOpacity style={{marginLeft:10}} onPress={()=>this.heart(item)}>
                {heart.length==0 && <Icon name="heart-o" type="FontAwesome" style={{color:'grey'}}/>}
                {heart.length!=0 && <Icon name="heart" type="FontAwesome" style={{color:'grey'}}/>}
                </TouchableOpacity>
            </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
            <TouchableOpacity style={{marginRight:10}} onPress={()=>{
              Clipboard.setString(this.state.offline==false? item.title.rendered:item.title)
              ToastAndroid.show('Text Copied to Clipboard !', ToastAndroid.SHORT)
            }}>
                <Icon name="copy" type="FontAwesome" style={{color:'grey'}}/>
            </TouchableOpacity>
        </View>
        {/* <View style={{flex:1}}></View> */}
    </View></Footer></Card>
    </TouchableOpacity>
    );},this)
          }
               
                </View>
              </View>
          </Content>
        </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop:StatusBar.currentHeight
  },
  sliderImage: {width: '100%', height: 160},
});