import React from 'react';
import {StyleSheet, Text, View, NetInfo, ScrollView, Image, TouchableOpacity,ImageBackground
  ,ActivityIndicator} from 'react-native'; 
import { Content, Container, Header, Textarea, Body, Right, Title ,Button} from 'native-base';
import {  createStackNavigator 
} from 'react-navigation';
import { Constants, SQLite } from 'expo';
var db = SQLite.openDatabase('db.db7');//starts db1
var self=''
class CaptionScreen extends React.Component {

  constructor (props) {
    super (props);
    self=this;
    this.state={allCategories:[],fetched:false, offline:false};
    this.fetchData=this.fetchData.bind(this);
    this.insertFunc=this.insertFunc.bind(this);
  }
  componentDidMount(){
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists category (id integer primary key not null,key integer, name text);'
      );
    });
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {
          console.warn('Internet is connected cap');
          this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.fetchData();
            }
          );
          return;
      }
      if(!isConnected)
      {
          console.warn('No Connection cap');
          db.transaction(
            tx => {
              tx.executeSql('select * from category', [], (_, { rows }) =>{
              console.log(JSON.stringify(rows));
              this.setState({allCategories:rows._array,fetched:true, offline:true})}
              );
            },
            null,
            console.log('success'));
      }
    })
    
  }
fetchData(){

  fetch('https://topliketags.com/wp-json/wp/v2/categories/?per_page='+100)
    .then(response => response.json())
    .then((response) => {
      console.log('captions');
      this.setState({allCategories:response,fetched:true})
      console.log(response.length);
      for(var i=0;i<response.length;i++){
        this.insertFunc(response[i].id,response[i].name)
      }
     })
    .catch((e) =>{
      console.log("error:");
      console.log(e);
    });
}
  render () { 
    return (
      <View style={styles.container}>
        <Container style={styles.container}>
          <Content padder style={styles.container}>
          <View  style={{flex:1 }}>
                <View padder style={{flex:1,flexWrap:'wrap',flexDirection:'row'
                ,justifyContent:'space-around',alignItems:'center'}}>
                {this.state.fetched===false && 
                  this.state.allCategories.length==0 &&
                  <ActivityIndicator size="large" color="#2fc2e3"/>}
                {this.state.fetched!==false && 
  this.state.allCategories.length!=0 && this.state.allCategories.map(function(item,key){
    return(
                <TouchableOpacity key={key} 
                onPress={() => this.props.navigation.navigate ('CaptionCategory', { catId: this.state.offline==false? item.id:item.key, 
                  catName:item.name })} 
                 style={{height:120,width:'45%',elevation:4,
                 shadowOffset: { width: 5, height: 5 },
                 shadowColor: "grey",
                 shadowOpacity: 0.5,padding:5,
                 shadowRadius: 10
                ,backgroundColor:'white',marginTop:7,}}
                ><ImageBackground
                  style={styles.sliderImage} 
                  source={require('../assets/back.png')}
                  ><View style={{position: 'absolute', top: 2, left: 2,
                 right: 2, bottom: 2, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{backgroundColor:'#ebebeb',fontWeight:'500',fontSize:16
                      ,borderColor:'#aeaeaf',borderRadius:4, padding:3,
                      borderWidth:0.5}}>{item.name}</Text>
              </View></ImageBackground>
                </TouchableOpacity>
    );},this)
          }
                
                </View>
              </View>
          </Content>
        </Container>
      </View>
    );
  }
  insertFunc(key,name){
    // console.log(key,'captions',name);
    db.transaction(tx => {
      tx.executeSql(
        `select * from category where key = ?;`,
        [key],
        (_, { rows : { _array } })  =>
        {
           console.log(_array.length)
           if(_array.length>0){
            //  console.log('no insertion => @ ',key)
           }
             if(_array.length==0){
               console.log('insert please',key)
            db.transaction(
              tx => {
                tx.executeSql('insert into category (key, name) values (?, ?)',
                [key,name]);            
              },
            );
          }
        }
      );
    });
    // db.transaction(
    //   tx => {
    //     tx.executeSql('insert into category (key, name) values (?, ?)',
    //     [key,name]);            
    //   },
    // );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  sliderImage: {flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'contain', },
});
export default Captions = createStackNavigator
(
  {
    CaptionScreen: {screen:CaptionScreen},
  },
  {
      initialRouteName: 'CaptionScreen'
    ,navigationOptions :{
      header: null,
    } 
  }
);