import React from 'react';
import {StyleSheet, Text, View, StatusBar, ScrollView, Image, Dimensions,ImageBackground
  ,TouchableOpacity} from 'react-native'; 
  import HomeScreen from './HomeScreen';
  import {  
    createMaterialTopTabNavigator,  createStackNavigator ,DrawerActions
  } from 'react-navigation';
  import {Icon} from 'native-base';
  import Captions from './Captions';
import Convert from './Convert';
import Hashtags from './Hashtags';
import Tips from './Tips'; import CaptionCategory from './CaptionCategory';
import CaptionPost from './CaptionPost';  
const TabsRoute = createMaterialTopTabNavigator (
    {
        Hashtags: {
        screen: Hashtags,
        navigationOptions: {
        //   header: null,
        //   headerVisible: false,
        tabBarLabel:({ focused }) => (<Text 
            style={{ fontSize: 17, color:focused ? 'white' : '#ebebeb',height:25}}>HASHTAG</Text>),
           
        },
      },
      Captions: {
        screen: Captions,
        navigationOptions: {
            tabBarLabel:({ focused }) => (<Text 
                style={{ fontSize: 17, color:focused ? 'white' : '#ebebeb',height:25}}>CAPTION</Text>)
           
        },
      },
      Convert: {
        screen: Convert,
        navigationOptions: {
            tabBarLabel:({ focused }) => (<Text 
                style={{ fontSize: 17, color:focused ? 'white' : '#ebebeb',height:25}}>CONVERT</Text>)
           
        },
      },
      Tips: {
        screen: Tips,
        navigationOptions: {
            tabBarLabel:({ focused }) => (<Text 
                style={{ fontSize: 17, color:focused ? 'white' : '#ebebeb',height:25}}>TIPS</Text>)
           
        },
      },
    },
    {
      swipeEnabled: true,
    //   headerMode: 'none',
      initialRouteName: 'Captions',
      tabBarOptions: {
        activeBackgroundColor: 'black',
        activeTintColor: 'white',
        inactiveTintColor: '#ebebeb',
        showIcon: false,
        showLabel: true,
        style: {
          //   marginTop: getStatusBarHeight (),
          backgroundColor: '#2fc2e3',
        },
        indicatorStyle: {
          backgroundColor: '#fff',
        },
      },
    }
  );
  

  


export default Drawer = createStackNavigator({
    DrawerTabs: {
      screen: TabsRoute,
      navigationOptions:  ({navigation}) => ({
        headerTitleStyle: {
          color:"white"
       },
       headerLeft: <View style={{marginLeft:20}}><TouchableOpacity onPress={() => 
         navigation.dispatch(DrawerActions.toggleDrawer())}><Icon
       type="Ionicons"
       name="md-menu"
       style={{color:'white'}}
       size={24}
     /></TouchableOpacity></View>,
        title: '   TopLikeTags',
        headerStyle: {
          backgroundColor: '#2fc2e3',
          borderBottomColor: '2fc2e3',
          color:"white",shadowColor: 'transparent',
          borderBottomWidth: 0, elevation:0,
      },
      headerRight:<View style={{color:"white",marginRight:20}}><TouchableOpacity
      onPress={() => navigation.navigate ('HomeScreen')}><Icon
      type="FontAwesome"
      name="home"
      style={{color:'white'}}
      size={24}
    /></TouchableOpacity></View>,
    style: { shadowColor: 'transparent' },
      }),
    },
    HomeScreen: {
      screen: HomeScreen 
    },  
    CaptionCategory: {screen:CaptionCategory},
    CaptionPost: {screen:CaptionPost} ,
  },
  {
    initialRouteName: 'DrawerTabs',
  });