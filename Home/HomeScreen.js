import React from 'react';
import {StyleSheet, Text, View, StatusBar, TouchableOpacity, ActivityIndicator,
  Clipboard, ToastAndroid} from 'react-native'; 
import {
  Header,
  Left,
  Body,
  Right,
  Footer,
  Button,
  Item,
  Input,
  Icon,
  Card,
  CardItem,
  Title,
  Container,
  Content,
} from 'native-base';
import { DrawerActions } from 'react-navigation'
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null 
  };
  constructor (props) {
    super (props);
    this.state={isReady:false, allPosts:[],fetched:false};
    this.fetchHome=this.fetchHome.bind(this);
  }
  async componentWillMount () {
    await Expo.Font.loadAsync ({
      Roboto: require ('../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({isReady: true});
  }
  componentDidMount(){
    // this.fetchData();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.fetchHome();
      }
    );
  }
  fetchHome(){
  fetch('http://www.topliketags.com/wp-json/wp/v2/posts?per_page=80')
    .then(response => response.json())
    .then((response) => {
      console.log('tips');
      this.setState({allPosts:response,fetched:true})
      // console.log(response );
      console.log(response.length);
     })
    .catch((e) =>{
      console.log("error:");
      console.log(e);
    });
}
  render () { 
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <View style={styles.container}>
      <Header style={{backgroundColor: '#2fc2e3',
          borderBottomColor: '2fc2e3',
          color:"white",shadowColor: 'transparent',
          borderBottomWidth: 0, elevation:0,}}>
      <Left>
        <Button onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}
         transparent>
          <Icon type="Ionicons"
       name="md-menu" style={{color:'white'}}
       size={24}/>
        </Button>
      </Left>
      <Body>
        <Title style={{fontWeight:'500',marginLeft:15,fontSize:21}}>TopLikeTags</Title>
      </Body>
      <Right>
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate ('DrawerTabs')}><Icon
        type="FontAwesome"
        name="home"
        style={{color:'white'}}
        size={24}
      /></TouchableOpacity>
      </Right>
    </Header>
    <Header  style={{backgroundColor: '#2fc2e3',
          borderBottomColor: '2fc2e3',
          color:"white",shadowColor: 'transparent',
          borderBottomWidth: 0, elevation:0,}}>
      <Body style={{flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between'
      }}><TouchableOpacity onPress={()=> this.props.navigation.navigate('Hashtags')}
        style={{width:'25%',alignItems:'center'}} 
      ><Text  style={{color:'#ebebeb',fontSize:17}}>HASHTAG</Text></TouchableOpacity> 
      <TouchableOpacity  onPress={()=> this.props.navigation.navigate('Captions')}
        style={{width:'25%',alignItems:'center'}} 
        ><Text  style={{color:'#ebebeb',fontSize:17}}>CAPTION</Text></TouchableOpacity> 
      <TouchableOpacity  onPress={()=> this.props.navigation.navigate('Convert')}
        style={{width:'25%',alignItems:'center'}} 
        ><Text  style={{color:'#ebebeb',fontSize:17}}>CONVERT</Text></TouchableOpacity> 
      <TouchableOpacity  onPress={()=> this.props.navigation.navigate('Tips')}
        style={{width:'25%',alignItems:'center'}} 
        ><Text  style={{color:'#ebebeb',fontSize:17}}>TIPS</Text></TouchableOpacity></Body>
    </Header>
    <Container>
      <Content padder>
      

        <View padder style={{flex:1,flexWrap:'wrap',flexDirection:'row'
                ,justifyContent:'space-between',alignItems:'center'}}>
                {this.state.fetched===false && 
                  this.state.allPosts.length==0 &&
                  <ActivityIndicator style={{justifyContent:'center',width:'100%'}} 
                  size="large" color="#2fc2e3"/>}
                  {this.state.fetched!==false && 
  this.state.allPosts.length==0 && <Text>No Posts Yet...</Text>}
                {this.state.fetched!==false && 
  this.state.allPosts.length!=0 && this.state.allPosts.map(function(item,key){
    return(<TouchableOpacity key={key} 
        onPress={() => this.props.navigation.navigate ('CaptionPost', { postId: item.id ,
        catName:'fetch'})} style={{height:150,width:'48%',  
}}
    >
    <Card style={{flex:1,borderRadius:8}}><Content padder style={styles.sliderImage } >
    <View style={{flex:1, flexDirection:"row",justifyContent:'center'
    ,alignItems:'center'}}>
    <Text style={{fontWeight:"500", color:'grey'}}>{item.title.rendered}</Text></View></Content>
    <View style={{height:50,flexDirection:'row',
            justifyContent:'space-between' }}>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-start'}}>
            </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
            <TouchableOpacity style={{marginRight:10}} onPress={()=>{
              Clipboard.setString(item.title.rendered)
              ToastAndroid.show('Text Copied to Clipboard !', ToastAndroid.SHORT)
            }}><Icon name="copy" type="FontAwesome" style={{color:'#2fc2e3'}}/>
            </TouchableOpacity>
        </View>
    </View></Card>
    </TouchableOpacity>
    );},this)
          }</View></Content>
    </Container>
  </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#2fc2e3',
  },
  sliderImage: {width: '100%', height: 100},
});