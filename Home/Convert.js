import React from 'react';
import {Font} from 'expo';
import {StyleSheet, Text, View,Clipboard, ToastAndroid , AsyncStorage} from 'react-native'; 
import { Content, Form, Header, Textarea, Body, Right, Title ,Button} from 'native-base';
export default class Convert extends React.Component {

  constructor (props) {
    super (props); 
    this.state = { 
      loading: true,
    }; 
    this.getData=this.getData.bind(this);
  }
  getData(){
    AsyncStorage.getItem ('convert', (err, result) => {
      if(result!=null){
        // console.log("fe",result)
        this.setState({textInputArea:result})
      }
    });
  }
  componentDidMount(){
    this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getData();
      }
    );
  }
  async componentWillMount () {
    await Font.loadAsync ({
      Roboto: require ('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require ('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState ({loading: false, textInputArea:''});
  }
 

  render () {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <View style={styles.container}>
      <Header style={{backgroundColor:'white'}}>
           
          <Body style={{justifyContent:'center',marginLeft:'45%'}}>
            <Title style={{color: '#2fc2e3',fontSize:26}}> #  .  -</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Form>
            <Textarea rowSpan={5} bordered placeholder="Enter Your Caption" 
            value={this.state.textInputArea} 
            onChangeText={(e)=>{this.setState({textInputArea:e});
            AsyncStorage.setItem("convert",this.state.textInputArea)}}/>
          </Form>
          <Text style={{marginTop:10,fontWeight:'500'}}>#TopLikeTags #TLT2019</Text>
          <View style={{width:'100%',justifyContent:'center'}}>
            <Button block  style={{marginTop:30,backgroundColor: '#2fc2e3'}}
            onPress={()=>{
              Clipboard.setString(this.state.textInputArea)
              ToastAndroid.show('Text Copied to Clipboard !'
              , ToastAndroid.SHORT)
            }}>
              <Text style={{fontWeight:'600',color:'white',fontSize:18}}>Convert</Text>
            </Button>
          </View>
          <Text style={{marginTop:30,fontWeight:'500',color:'#b8b8b8'}}>Dont forget to follow us at @TopLikeTags</Text>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
});