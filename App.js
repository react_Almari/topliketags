import React from 'react';
import {StyleSheet,TouchableOpacity,View, Alert, Linking, Platform
  , Text, SafeAreaView,ScrollView,Image}
   from 'react-native';
import { 
  createDrawerNavigator,
  DrawerItems,
  createSwitchNavigator,
  createStackNavigator,createAppContainer
} from 'react-navigation';
import Drawer from './Home/Drawer';
import Favorite from './Home/Favorite';
class CustomDrawerComponent extends React.Component {
 
 render () {  
   
   var o=this.props;
   return (
     <SafeAreaView style={{flex:1}}> 
       <View   style={{ backgroundColor:'#ccf8f5',width:'100%',
         height:210,flexDirection:"row",justifyContent:"center",alignItems:'center' }}>
          
       <Image source={{uri:'https://www.topliketags.com/wp-content/uploads/2018/12/color2-copy.png'}} style={{  
        height:200,width:200, resizeMode:'contain'}}/> 
       </View>
       <ScrollView>
         <DrawerItems {...o} />
           <TouchableOpacity 
         onPress={()=>{Alert.alert(
          'TopLikeTags',
          'Please rate it on Store.',
          [
            {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')}, 
            {text: 'OK', onPress: () => {
              var url='';
              if (Platform.OS === 'ios') {
                  url =
                  'itms://itunes.apple.com/pk/app/facebook/id284882215?mt=8';
              } else {
                  url = 'market://details?id=com.bakralhumaid.PocketATM';
              }
  
              Linking.canOpenURL (url)
                .then (supported => {
                  if (!supported) {
                    // console.warn ("Can't handle url: " + url);
                  } else {
                    return Linking.openURL (url);
                  }
                })
                .catch (err =>
                   console.warn ('An error occurred', err));

            }
          },
          ],
          {cancelable: false},
        );}} >
          
         <Text style={{marginLeft: 16,marginTop:8,
   fontWeight: 'bold',
   color: 'rgba(0, 0, 0, .99)',}}>Rate App</Text>
       </TouchableOpacity> 
       </ScrollView>
     </SafeAreaView> 
     );
   }
}

const MyApp = createDrawerNavigator (
  {  
    Drawer: {
      screen: Drawer,
    },
    Favorite: {
      screen: Favorite,
    },
   
  },
  { drawerLockMode: 'locked-closed',
    contentComponent:CustomDrawerComponent,
    initialRouteName: 'Drawer',
  }
);

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default class App extends React.Component {
  constructor (props) {
    super (props);
  }
  render () {
    return (<MyApp screenProps={this.props} />);
  }
}